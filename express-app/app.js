var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');
const passport = require('passport');
var expressSession = require('express-session');
var login = require('./routes/login')(passport);
var socialRouter = require('./routes/social')(passport);
var social2 = require('./routes/social2')(passport);
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var newsRouter = require('./routes/news');
var logoutRouter = require('./routes/logout');


var bodyParser = require('body-parser');
require('./passport/passport')(passport);
require('./passport/passport-social')(passport);


var app = express();


app.use((req, res, next) => {
  // const { origin } = req.headers;
  res.header("Access-Control-Allow-Origin", "http://192.168.1.238:3000");
  // res.header("Access-Control-Allow-Origin", "http://localhost:3000");
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Content", "application/json");
  next();
});
// var loginRouter = require('./routes/login');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.use(logger('dev'));
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(require('cookie-parser')());
app.use(expressSession({ secret: 'mySecretKey', resave: false, saveUninitialized: false,}));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());


app.use('/users', usersRouter);
app.use('/news', newsRouter);
app.use('/login', login);
app.use('/social',socialRouter);
app.use('/social2',social2);
app.use('/logout',logoutRouter);
app.use('/', indexRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
