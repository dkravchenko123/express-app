var express = require('express');
var router = express.Router();
var multer  = require('multer');
// var upload = multer({ dest: 'images/' });
let news = require('../databases/news');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/images')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
});
var upload = multer({ storage: storage })

router.get('/',function(req,res,next){
  news.find({}).then(result => res.send(result));
});
router.post('/', upload.single('file'),function(req,res){
  let newsCard = {...req.body};
  newsCard.date = new Date().toLocaleString();
  console.log('newsCard.date --> ', newsCard.date);
  if(req.file!==undefined) newsCard.image = 'http://192.168.1.238:5000/'+'images/'+req.file.filename;
  console.log('newsCard.image --> ', newsCard);
  // console.log('req.body --> ', req.body);
  //  if(req.body.file===undefined && req.file===undefined) news.create({title:req.body.title,text:req.body.text,tags:req.body.tags,user_id:req.body.user_id,image:''}).then(result=>res.send(result));
    news.create(newsCard).then(result=>res.send(result));
});
router.delete('/:id',function(req,res){
  console.log(req.params);
  news.deleteOne({_id:req.params.id},function(err){console.log(err)})
  res.send('ok');
})

module.exports = router;
