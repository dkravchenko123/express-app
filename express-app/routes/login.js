var express = require('express');
var router = express.Router();
const user = require('../databases/user');

module.exports = function (passport) {

  router.post('/', passport.authenticate('local', {session:true,
      failureRedirect: '/',
    }),
    function (req, res) {
      console.log(req.session.cookie)
      return res.send({ user:req.user, cookie:req.session.cookie });
    });

  return router;
};




// router.post('/',function(req,res){
//   console.log(req.body);
//   user.findOne({login:req.body.login, password:req.body.password})
//   .then((result)=>{console.log(result);res.send(result)})
//
// });
//
// module.exports = router;


