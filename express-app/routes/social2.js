var express = require('express');
var router = express.Router();
const user = require('../databases/user');
const passport = require('passport');

module.exports = function(passport){

  router.get('/auth/google/callback',
    passport.authenticate('google', { failureRedirect: '/login' }),
    function(req, res) {
      // Successful authentication, redirect home.
      res.redirect('/social');
    });
    return router;
};

