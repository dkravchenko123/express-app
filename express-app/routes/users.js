var express = require('express');
var router = express.Router();
let user = require('../databases/user');
var multer  = require('multer');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/images')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
});
var upload = multer({ storage: storage })
/* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });
//
router.get('/',function(req,res,next){
  user.find({}).then(result => {res.send(result);});
});

router.post('/',function(req,res,next){
  user.create(req.body)
  res.send(req.body)
});
router.patch('/',upload.single('file'),function(req,res){
  let userCard = {...req.body};
  if(req.file!==undefined) userCard.image = 'http://192.168.1.238:5000/'+'images/'+req.file.filename;
  user.findOneAndUpdate({_id: req.body.id},{ name:req.body.name,surname:req.body.surname,age:req.body.age,password:req.body.password,username:req.body.username,avatar:userCard.image })
    .then(result=>res.send(result));
})
//
// router.get('/',function(req,res,next){
//   news.find({}).then(result => {res.send(result);});
// });
// router.post('/',function(req,res,next){
//   console.log(req.body);
//   news.create(req.body)
//     .then(result => {
//       res.send(result)
//     })
// })

module.exports = router;
