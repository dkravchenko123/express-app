const LocalStrategy = require('passport-local').Strategy;
const User = require('../databases/user');



module.exports = function (passport) {

  passport.use(new LocalStrategy({
    usernameField: 'login',
    passwordField: 'password',
    session: true
  }, function (username, password, done) {
    User.findOne({username: username}, function (err, user) {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false, {message: 'Incorrect username.'});
      }
      if (user.password !== password) {
        return done(null, false, {message: 'Incorrect password.'});
      }
      return done(null, user);
    });
  }));

  passport.serializeUser(function (user, done) {
    console.log(1);
    done(null, user._id);
  });
  passport.deserializeUser(function (user, done) {
    console.log(user);
    User.findById(user._id, function(err,user){done(err, user)})
  });
};
