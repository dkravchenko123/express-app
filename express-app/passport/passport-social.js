
var passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const User = require('../databases/user');
const { GOOGLE_CLIENT_ID , GOOGLE_CLIENT_SECRET} = require('../config/config');

module.exports = function(passport){

passport.use(new GoogleStrategy({
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    callbackURL: "http://192.168.1.238/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
    User.findOrCreate({ googleId: profile.id }, function (err, user) {
      return cb(err, user);
    });
  }
));
};


