let mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/users', { useNewUrlParser: true });
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('connect news');
});

let newsSchema = new mongoose.Schema ({
  title: String,
  text: String,
  tags:String,
  user_id: '',
  image:String,
  date:String,
});
let news = mongoose.model('news',newsSchema);

// news.create({title:'fsdfsdfsd',text:'fdsfsdfsdfsdfdsfdsfsdfsdfsdf',user_id:'123213'});

module.exports = news;
