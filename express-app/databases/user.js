let mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/users', { useNewUrlParser: true });
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('connect users');
});

let usersSchema = new mongoose.Schema ({
  name: String,
  surname: String,
  age: Number,
  password: String,
  username: String,
  avatar:String
});
let user = mongoose.model('user',usersSchema);

// user.create({name:'1eqweqweqw',surname:'sdfdsfdsfsd',age:23,password:'fddfgdfg'});

module.exports = user;
